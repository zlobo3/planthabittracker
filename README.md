# "Don't Kill My Plant" Habit Tracker

<img src="notebook/zade/pictures/enclosure.jpeg" alt= “Enclosure” height="800px">

<img src="notebook/zade/pictures/team.jpeg" alt= "The Team" height="500px">

## Team Members

Zade Lobo

Ben Wei

Dike Ehiribe

## Problem

We are trying to solve a problem that has plagued people for ages: breaking bad habits and adopting good ones. Even though humans may want to change these habits, they usually lack the willpower in order to do so. 

Common solutions on the market to help people change habits include smartphone tracking apps and physical devices that track physical habits. These solutions are great for tracking, but the
There are plenty of apps and devices that help people with habit tracking, but most of them can be circumvented easily and don't hold people accountable for their actions. In addition, any positive reinforcement methods that they provide are minor and are not effective enough. 

## Solution

We want to change this by bringing in emotional attachment to tangible consequences to encourage people to keep up with their habits. While positive reinforcement may not be as effective, negative punishment has also shown promising results. 

"Don't Kill My Plant" is an application interface that will keep track of your habits through unforgeable data and will make the life of your plant dependent on it through hardware. 

The solution we are providing is innovative by causing people to emotionally attach themselves to keeping up with good habits as well as keeping a physical and visual reminder. 


## Solution Components

### Application Interface Subsystem

The application interface is a phone application that will offer multiple ways to track habit forming, including location, screentime, and message and call tracking. This allows the application to pick up on habits such as going to the gym, avoiding a coffee shop, spending too much time on social media, or messaging your family. 

### Plant Enclosure Subsystem

The plant enclosure is a box with an airtight lid in order to create an isolated environment for a plant. The box itself will contain transparent walls with a method for blocking light out (either electronic tint or rolling window shades. The box will have an airtight lid that can be electronically opened and closed by the microcontroller. 

### Microcontroller Subsystem

The microcontroller will be an ESP32 or ESP8266 that will pull from a server that the application interface is publishing to. This will determine the binary state of the plant enclosure system (killing or living). When the state on the server changes, then the microcontroller will control the plant enclosure subsystem to change the state of the box. The microcontroller will also allow routine watering of the plant through the irrigation system, which may be interrupted by not keeping up with habits. 

### Irrigation Subsystem

The irrigation subsystem will be controlled by the microcontroller in order to routinely water the plant through the included water solenoid. The reservoir outside the plant enclosure will allow the user to input water for irrigation, but the actual water delivery will be controlled through hydraulic tubing piping it inside the system. 

## High-Level Requirements
- [ ] The application interface is able to facilitate habit tracking for the user and send this information to the physical device. 

- [x] The created device system is capable of keeping a potted plant alive and killing a potted plant according to the data passed to it. 

- [x] The design has a modular design, allowing for more plants-enclosure addons to track more habits.
