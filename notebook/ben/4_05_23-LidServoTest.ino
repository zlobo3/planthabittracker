//Lid Test

#include <Servo.h>

int lid_spd = 60;
int lid_duration = 1000;

Servo PWM5;

void setup() {
  PWM5.attach(6,1300,1700);
}

void loop() {
  delay(1000);
  DeployLid();
  delay(1000);
  RecallLid();
}

void DeployLid(){
  PWM5.write(90+lid_spd);
  delay(lid_duration);
  PWM5.write(90);
}

void RecallLid(){
  PWM5.write(90-lid_spd);
  delay(lid_duration);
  PWM5.write(90);
}