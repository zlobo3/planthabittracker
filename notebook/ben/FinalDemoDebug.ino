//written by ben

#include <Servo.h>
#include <SoftwareSerial.h>

int servospeed = 60;
int duration = 10000;

int lidspeed = 60;
int lidduration = 2000;

int waterduration = 10000;

int buttonPin = 8;

// 0=unlocked, 1=locked
int state = -1;

String ssid = "ScaledAndIcy";
String pw = "Form!d@b1e";

Servo PWM1;
Servo PWM2;
Servo PWM3;
Servo PWM4;

Servo Lid;

void setup() {
  pinMode(buttonPin, INPUT_PULLUP); //setup button
  pinMode(4, OUTPUT); //setup solenoid
  PWM1.attach(6, 1520-200, 1520+200);
  PWM1.writeMicroseconds(1520);
  PWM2.attach(10, 1520-200, 1520+200);
  PWM2.writeMicroseconds(1520);
  PWM3.attach(3, 1520-200, 1520+200);
  PWM3.writeMicroseconds(1520);
  PWM4.attach(5, 1520-200, 1520+200);
  PWM4.writeMicroseconds(1520);
  Lid.attach(9, 1520-200, 1520+200);
  Lid.writeMicroseconds(1520);

  /*
  //Wifi setup and config
  pinMode(0, INPUT);
  pinMode(1, OUTPUT);
  ESP.begin(115200);
  delay(1000);
  ESP.println("AT+CWJAP=\""+ssid+"\",\""+pw+"\""); //This is AT+CWJAP="ssid","pw"/r/n
  delay(1000);

  digitalWrite(4, HIGH);*/
}

void loop()
{
  
  int buttonValue = digitalRead(buttonPin);
  if (buttonValue == LOW)
  {
    if (state == 1)
    {
      RecallAll();
      state = 0;
    }
    else if (state == -1)
    {
      Initialize();
      state = 0;
    }
    else
    {
      DeployAll();
      state = 1;
    }
  }
}
/*
String readOut(){
  String output = "";
  while(ESP.available())
  {
    
  }
  return output;
}*/

void DeployAll()
{
  DeployLid();
  PWM1.writeMicroseconds(1520+servospeed);
  PWM2.writeMicroseconds(1520+servospeed);
  PWM3.writeMicroseconds(1520+servospeed);
  PWM4.writeMicroseconds(1520+servospeed);
  delay(8000);
  PWM1.writeMicroseconds(1520);
  PWM2.writeMicroseconds(1520);
  PWM3.writeMicroseconds(1520);
  PWM4.writeMicroseconds(1520);
}

void RecallAll()
{
  RecallLid();
  PWM1.writeMicroseconds(1520-servospeed);
  PWM2.writeMicroseconds(1520-servospeed);
  PWM3.writeMicroseconds(1520-servospeed);
  PWM4.writeMicroseconds(1520-servospeed);
  delay(15000);
  PWM1.writeMicroseconds(1520);
  PWM2.writeMicroseconds(1520);
  PWM3.writeMicroseconds(1520);
  PWM4.writeMicroseconds(1520);
  ActivateSolenoid();
}

void RecallLid()
{
  Lid.writeMicroseconds(1520+60);
  delay(1000);
  Lid.writeMicroseconds(1520);
}

void DeployLid()
{
  Lid.writeMicroseconds(1520-20);
  delay(10000);
  Lid.writeMicroseconds(1520+60);
  delay(230);
  Lid.writeMicroseconds(1520);
}

void Initialize()
{
  //reset lid
  Lid.writeMicroseconds(1520-20);
  delay(10000);
  Lid.writeMicroseconds(1520+60);
  delay(1230);
  Lid.writeMicroseconds(1520);

  //reset curtains
  PWM1.writeMicroseconds(1520-servospeed);
  PWM2.writeMicroseconds(1520-servospeed);
  PWM3.writeMicroseconds(1520-servospeed);
  PWM4.writeMicroseconds(1520-servospeed);
  delay(15000);
  PWM1.writeMicroseconds(1520);
  PWM2.writeMicroseconds(1520);
  PWM3.writeMicroseconds(1520);
  PWM4.writeMicroseconds(1520);
}

void ActivateSolenoid()
{
  digitalWrite(4, HIGH);
  delay(waterduration);
  digitalWrite(4, LOW);
}
