//SolenoidTest

//How long does it take for the solenoid to actuate? Pretty fast?

void setup() {
  pinMode(4, OUTPUT);
}

void loop() {
  delay(5000);
  EnableSolenoid();
  delay(5000);
  DisableSolenoid();
}

void EnableSolenoid(){
  digitalWrite(4, HIGH);
}

void DisableSolenoid(){
  digitalWrite(4, LOW);
}