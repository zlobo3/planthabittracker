//written by ben

#include <Servo.h>

int speed = 60;
int duration = 5000;

int lidspeed = 60;
int lidduration = 2000;

Servo PWM1;
Servo PWM2;
Servo PWM3;
Servo PWM4;

Servo Lid;

void setup() {
  PWM1.attach(6, 1520-200, 1520+200);
  PWM1.writeMicroseconds(1520);
  PWM2.attach(10, 1520-200, 1520+200);
  PWM2.writeMicroseconds(1520);
  PWM3.attach(3, 1520-200, 1520+200);
  PWM3.writeMicroseconds(1520);
  PWM4.attach(5, 1520-200, 1520+200);
  PWM4.writeMicroseconds(1520);
  Lid.attach(9, 1520-200, 1520+200);
  Lid.writeMicroseconds(1520);

  delay(1000);
  RecallLid();
  delay(1000);
  DeployLid();
}

void loop() {
  //delay(1000);
  //DeployAll();
  //delay(1000);
  //RecallAll();
}

void DeployAll(){
  DeployLid();
  PWM1.writeMicroseconds(1520+speed);
  PWM2.writeMicroseconds(1520+speed);
  PWM3.writeMicroseconds(1520+speed);
  PWM4.writeMicroseconds(1520+speed);
  delay(duration);
  PWM1.writeMicroseconds(1520);
  PWM2.writeMicroseconds(1520);
  PWM3.writeMicroseconds(1520);
  PWM4.writeMicroseconds(1520);
}

void RecallAll(){
  RecallLid();
  PWM1.writeMicroseconds(1520-speed);
  PWM2.writeMicroseconds(1520-speed);
  PWM3.writeMicroseconds(1520-speed);
  PWM4.writeMicroseconds(1520-speed);
  delay(duration);
  PWM1.writeMicroseconds(1520);
  PWM2.writeMicroseconds(1520);
  PWM3.writeMicroseconds(1520);
  PWM4.writeMicroseconds(1520);
}

void DeployLid(){
  Lid.writeMicroseconds(1520+60);
  delay(1230);
  Lid.writeMicroseconds(1520);
}

void RecallLid(){
  Lid.writeMicroseconds(1520-20);
  delay(12000);
  Lid.writeMicroseconds(1520);
}