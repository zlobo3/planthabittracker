# January 21, 2023
I just met both of my groupmates: Dike and Ben. We met at my research lab to discuss possible projects that interest us. We came down to a couple of ideas including an automated vivarium for plants, something that can help people keep up with good habits, and a pen for better user interaction with computers. We agreed to combine two of the ideas by connecting people's habits to keeping a plant alive. 

# January 24, 2023
I have written a basic RFA in order to combine the two ideas, and I have gotten feedback from the team. It has been submitted for approval. 

# January 26, 2023
We have responded to the comments made about our RFA, and it has been approved. 

# February 4, 2023
We are working on our design proposal as well as trying to determine feasibility for the entire project. We have determined three things that are necessary for plants to live and are trying to develop ways to control those. 

# February 6, 2023
After finishing up our proposal, we have submitted it. We had a slight dilemma with voltage values needed to drive our water solenoid, but we will determine specifics later. 

# February 15, 2023
I have made a spreadsheet of all the items we would need to buy as well as specific links we will buy them on and how much it would cost for backup items. 

I have also created a CAD in Autodesk Inventor to show how all the systems will fit together with off-the-shelf materials. 

# February 19, 2023
I have fractured my arm, but I need to keep working for the sake of my team. I rebound a bunch of buttons so that I can keep working on the 3D CAD. I was able to design custom 3D printed brackets and hinges to put a bunch of parts together. 

# February 23, 2023
I decided to work on the CAD schematic for the board. I got all the pins set up and working, but I passed off routing to Ben. I had to do a ton of research to figure out the pinouts for most of the components.

# February 25, 2023
The team has compiled everything into the design document as well as a new tolerance analysis. 

# March 3, 2023
I have put in orders for a lot of the parts that are needed. These orders were sent out to Amazon, Digikey, and The Home Depot, totalling around $250. 

# March 7, 2023
Ben finished up the routing for the CAD and we have audited the Gerber files. Everything looks good, so we sent them off to the TA to be printed. We are a little worried due to the long turnaround time for the boards because mistakes can be made. 


# March 23, 2023
I just went to the shop at SCD for an hour to convert some of the raw materials to parts we need. This is mainly the acrylic for the base that needed to be cut to size and the steel shafts that needed to be cut to pieces. I have cut these parts and am bringing them home for assembly. 

# March 25, 2023
I picked up the first iteration of the 3D printed mount today for the motors and bearings. The clearance for the bearing was too small, but the motor fit right. I bought 10-24 screws for the motors so that they can attach once I install them. 

I also worked on the initial assembly of the enclosure. Getting the polycarbonate to stick to the base acrylic without a carpenter’s square was tricky, but I got it to work. The silicone is able to hold up enough weight, and the enclosure is pretty square. 

# March 26, 2023
I modified the model for the 3D print, and I sent the files to my printer to try again. In the end, I stuck with the very tight clearance, but it was good to test it. 

# March 27, 2023
I 3D printed potential ways to attach the steel shafts to the servo motors. After 2 hours of frustration with how the nub on the servo was designed, I decided to just tape them together. 

I received the screws needed in the mail, so I attached the servo motors and bearings to the 3D printed parts. I have started attaching them to the enclosure. 

# April 2, 2023
Soldered up most of the components on the board. As it turns out, the SMD purchased appear to be the wrong size, so I will need to ask someone how to get the right sized components. 

# April 10, 2023
Finished soldering in the small components on the board. I went to the lab to borrow a USB programmer to test out the programs Ben sent me, but they no longer have any programmers. I have ordered a couple, and they should be here tomorrow or the day after. 

# April 11, 2023
We decided to test out the power subsystem without having the USB programmer, and we were getting very funny voltage values. It seemed like the actual wall brick was broken, but after a few hours of debugging, we found that the footprint for the barrel jack that we put on the PCB threw us off when we soldered the component. The voltage was being given in reverse, so we fried most of the regulators. 

# April 14, 2023
I soldered a completely new PCB with fresh components. Due to the reverse polarity issue we had last time, I didn't want to risk fried components. The only components I reused were the diode, the resistors, and the capacitors. 

I purchased new components from Digikey so that we have backups in case we need them. They should be here next week. 

# April 15, 2023
Ben and Dike came over to work on the integration of all the components into the system. We glued the lid onto the box with its hinges and glued in the last servo for the airlock subsystem. 

Ben attempted burning a bootloader onto the ATMega, but we were receiving an error that summed up to the programmer could not connect to the MCU. After hours of debugging, we decided to go to office hours to solve the issue. 

# April 17, 2023
After spending a few hours in office hours, we found that we were missing a connection between the reset pin of the MCU and the reset signal from the programmer. We soldered a wire between them to fix this and burned the bootloader. 

# April 19, 2023
I took the enclosure to my research lab to drill a hole in it. I needed a hole on the bottom to feed the cables through when we did cable management, as well as a hole for the water reservoir to feed into the water solenoid. 

# April 20, 2023
I spent some time finishing up the fabrication of the box. I did some cable management to get the cables out of the way, and I used epoxy to start putting together the irrigation system. 

# April 23, 2023
The team got together to finish the integration and code the implementation. We finished rigging together the water solenoid and mounted the PCB to the bottom of the enclosure. We then plugged in all of the cables and coded the system. 

We had trouble integrating the wireless module with the MCU, so we spent a long time debugging it. We removed a resistor because its resistance value was too high, but we still could not communicate with the module. In the end, that was the last component we could not finish. For the demo, we soldered on a button as a debug signal so we can show the rest of the functionality working. 